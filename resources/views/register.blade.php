<html>
    <head>
        <title>Buat Account Baru!</title>
        <h1>Buat Account Baru!</h1>
    </head>
    <body>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
            <label for="firstname"> First name: </label><br><br>
            <input type="text" name="firstname"><br><br>
    
            <label for="lastname"> Last name: </label><br><br>
            <input type="text" name="lastname"><br><br>
            
            <label> Gender: </label> <br><br>
            <input type="radio" name="gender" value="0"> Male <br>
            <input type="radio" name="gender" value="1"> Female <br>
            <input type="radio" name="gender" value="2"> Other <br><br>
    
            <label> Nationality </label><br><br>
            <select name="" id="">
                <option value="indonesian"> Indonesian </option>
                <option value="singaporean"> singaporean </option>
                <option value="malaysian"> malaysian </option>
                <option value="australian"> australian </option>
            </select><br><br>
    
            <label> Language Spoken: </label> <br><br>
            <input type="checkbox" name="language_spoken" value="0"> Bahasa Indonesia <br>
            <input type="checkbox" name="language_spoken" value="1"> English <br>
            <input type="checkbox" name="language_spoken" value="2"> Other <br><br>
    
            <label> Bio: </label> <br><br>
            <textarea cols="40" rows="10"></textarea><br>
            <button type="submit" formaction="welcome">Sign Up</button>
        </form>
    </body>
</html>